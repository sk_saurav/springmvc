package com.app.mvc.common.cache;

public interface IStartUp {
    boolean initialize();
}
