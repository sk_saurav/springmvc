package com.app.mvc.common.cache;

import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.app.mvc.dao.StaticCacheConfigDao;
import com.app.mvc.pojo.UserInfo;

@Component("staticCache")
public class StaticCacheBean implements IStartUp {
	private Map<String, UserInfo> userCacheMap = null;

	@Autowired
	private StaticCacheConfigDao staticCacheConfigDao;

	@Override
	@PostConstruct
	public boolean initialize() {
		boolean result = false;
		result = initializeUserCache();
		return result;
	}

	private boolean initializeUserCache() {
		Map<String, UserInfo> tempUserMap = new HashMap<>();
		List<UserInfo> userInfoList = staticCacheConfigDao.findAllUserdata();
		if (!CollectionUtils.isEmpty(userInfoList)) {
			tempUserMap = userInfoList.stream().collect(toMap(UserInfo::getUserID, user -> user));
		}
		userCacheMap = tempUserMap;
		if (!CollectionUtils.isEmpty(userCacheMap)) {
			return true;
		}
		return false;
	}

	public List<UserInfo> getUserInfoList() {
		return new ArrayList<>(userCacheMap.values());
	}

	public UserInfo getUserByUserID(String userID) {
		return userCacheMap.get(userID);
	}
}
