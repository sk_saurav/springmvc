package com.app.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.mvc.common.cache.StaticCacheBean;
import com.app.mvc.pojo.UserInfo;
import com.app.mvc.service.UserService;

/**
 * created by saurav
 */
@Controller
@RequestMapping("")
public class HelloController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String printHello (ModelMap modelMap) {
        modelMap.addAttribute("message"," hello spring mvc framework");
        return "hello";
    }

    @ResponseBody
    @RequestMapping("/findUserById/{userID}")
    private UserInfo findUserById (@PathVariable final String userID) {
        return userService.findUserById(userID);
    }

    @Autowired
    private StaticCacheBean staticCacheBean;

    @ResponseBody
    @RequestMapping("/findById/{userID}")
    private UserInfo findById (@PathVariable final String userID) {
        return staticCacheBean.getUserByUserID(userID);
    }
}
