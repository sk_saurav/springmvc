package com.app.mvc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.app.mvc.pojo.UserInfo;

@Repository
public interface StaticCacheConfigDao {
    UserInfo findUserById(@Param("userID") String userID);

    List<UserInfo> findAllUserdata();
}
