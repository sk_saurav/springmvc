package com.app.mvc.service;

import com.app.mvc.pojo.UserInfo;

public interface UserService {
    UserInfo findUserById(String userID);
}
