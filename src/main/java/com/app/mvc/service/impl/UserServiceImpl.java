package com.app.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.mvc.dao.StaticCacheConfigDao;
import com.app.mvc.pojo.UserInfo;
import com.app.mvc.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private StaticCacheConfigDao staticCacheConfigDao;


    @Override
    public UserInfo findUserById(String userID) {
        return staticCacheConfigDao.findUserById(userID);
    }
}
